from django.db.models.query import QuerySet
from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from .forms import NewGrantGoalForm, UpdateGrantGoalForm

# Create your views here.

from core import models
from core import forms

### CRUD GG


### CREATE
class NewGrantGoal(generic.CreateView):
    template_name = 'core/grantgoal/create_gg.html'
    model = models.GrantGoal
    form_class = NewGrantGoalForm
    success_url = reverse_lazy('core:list_gg')

### RETRIEVE
### List
class ListGrantGoal(generic.View):
    template_name = "core/grantgoal/list_gg.html"
    context = {}

    def get(self, request):
        self.context = {
            "grantgoals": models.GrantGoal.objects.filter(status=True)
        }
        return render(request, self.template_name, self.context)

### Detail
class DetailGrantGoal(generic.View):
    template_name = "core/grantgoal/detail_gg.html"
    context = {}

    def get(self, request,pk):
        self.context = {
            "grantgoals": models.GrantGoal.objects.get(pk=pk)
        }
        return render(request, self.template_name, self.context)

### UPDATE
class UpdateGrantGoal(generic.UpdateView):
    template_name = "core/grantgoal/update_gg.html"
    model = models.GrantGoal
    form_class = UpdateGrantGoalForm
    success_url = reverse_lazy("core:list_gg")

### DELETE
class DeleteGrantGoal(generic.CreateView):
    template_name = 'core/grantgoal/delete_gg.html'
    model = models.GrantGoal
    success_url = reverse_lazy('core:list.gg')


### CRUD SUBGG

### CREATE
class NewSubGrantGoal(generic.CreateView):
    template_name = "core/subgrantgoal/create_sgg.html"
    model = models.SubGrantGoal
    form_class = forms.NewSubGrantGoalForm
    success_url = reverse_lazy("core:list_sgg")

### RETRIEVE
### List
class ListSubGrantGoal(generic.ListView):
    template_name = "core/subgrantgoal/list_sgg.html"
    queryset = models.SubGrantGoal.objects.filter(status=True)

### Detail
class DetailSubGrantGoal(generic.DetailView):
    template_name = "core/subgrantgoal/detail_sgg.html"
    model = models.SubGrantGoal
    
### UPDATE
class UpdateSubGrantGoal(generic.UpdateView):
    template_name = "core/subgrantgoal/update_sgg.html"
    model = models.SubGrantGoal
    form_class = forms.UpdateSubGrantGoalForm
    success_url = reverse_lazy("core:list_sgg")

### DELETE
class DeleteSubGrantGoal(generic.DeleteView):
    template_name = "core/subgrantgoal/delete_sgg.html"
    model = models.SubGrantGoal
    success_url = reverse_lazy("core:list_sgg")



# # # # #  C R U D G O A L S # # # # #


# # # C R E A T E # # #
class NewGoal(generic.CreateView):
    template_name = 'core/goal/create_g.html'
    model = models.Goal
    form_class = forms.NewGoalForm
    success_url = reverse_lazy('core:list_g')

# # # R E T R I E V E # # #
# # List
class ListGoal(generic.ListView):
    template_name = "core/goal/list_g.html"
    queryset = models.Goal.objects.filter(status=True)

# # Detail
class DetailGoal(generic.DetailView):
    template_name = "core/goal/detail_g.html"
    model = models.Goal

# # # U P D A T E # # #
class UpdateGoal(generic.UpdateView):
    template_name = "core/goal/update_g.html"
    model = models.SubGrantGoal
    form_class = forms.UpdateGoalForm
    success_url = reverse_lazy("core:list_g")


# # # D E L E T E # # #
class DeleteGoal(generic.DeleteView):
    template_name = "core/goal/delete_g.html"
    model = models.Goal
    success_url = reverse_lazy("core:list_g")


# # # # #  C R U D I S S U E S  # # # # #


# # # C R E A T E # # #
class NewIssue(generic.CreateView):
    template_name = 'core/issue/create_i.html'
    model = models.Issue
    form_class = forms.NewIssueForm
    success_url = reverse_lazy('core:list_i')

# # # R E T R I E V E # # #
# # List
class ListIssues(generic.ListView):
    template_name = "core/issue/list_i.html"
    queryset = models.Issue.objects.filter(status=True)

# # Detail
class DetailIssue(generic.DetailView):
    template_name = "core/issue/detail_i.html"
    model = models.Issue


# # # U P D A T E # # #

class UpdateIssue(generic.UpdateView):
    template_name = "core/issue/update_i.html"
    model = models.Issue
    form_class = forms.UpdateIssueForm
    success_url = reverse_lazy("core:list_i")

# # # D E L E T E # # #
class DeleteIssue(generic.DeleteView):
    template_name = "core/issue/delete_i.html"
    model = models.Issue
    success_url = reverse_lazy("core:list_i")


# # # # #  C R U D A R E A S # # # # #


# # # C R E A T E # # #
class NewArea(generic.CreateView):
    template_name = 'core/area/create_a.html'
    model = models.Issue
    form_class = forms.NewAreaForm
    success_url = reverse_lazy('core:list_a')

# # # R E T R I E V E # # #
# # List
class ListArea(generic.ListView):
    template_name = "core/area/list_a.html"
    queryset = models.Issue.objects.filter(status=True)
# # Detail
class DetailArea(generic.DetailView):
    template_name = "core/area/detail_a.html"
    model = models.Area


# # # U P D A T E # # #
class UpdateArea(generic.UpdateView):
    template_name = "core/area/update_a.html"
    model = models.Area
    form_class = forms.UpdateAreaForm
    success_url = reverse_lazy("core:list_a")


# # # D E L E T E # # #
class DeleteArea(generic.DeleteView):
    template_name = "core/area/delete_a.html"
    model = models.Area
    form_class = forms.UpdateAreaForm
    success_url = reverse_lazy("core:list_a")