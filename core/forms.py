from django import forms

from core import models

class NewGrantGoalForm(forms.ModelForm):
    class Meta:
        model = models.GrantGoal
        fields = [
            "ggname",
            "user",
            "description",
            "days_duration",
            "state",
            "priority",
            "slug"
        ]
        widgets = {
            "ggname": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el grantgoal name!"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "row": 3, "placeholder":"Escribe el grantgoal description!"}),
            "user": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "priority": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el grantgoal slug!"}),
        }



class UpdateGrantGoalForm(forms.ModelForm):
    class Meta:
        model = models.GrantGoal
        fields = [
            "ggname",
            "user",
            "description",
            "days_duration",
            "state",
            "priority",
            "slug"
        ]
        widgets = {
            "ggname": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el grantgoal name!"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "row": 3, "placeholder":"Escribe el grantgoal description!"}),
            "user": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "priority": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el grantgoal slug!"}),
        }

###subgrantgoal forms
class NewSubGrantGoalForm(forms.ModelForm):
    class Meta:
        model = models.SubGrantGoal
        fields = [
            "sggname",
            "user",
            "area",
            "grantgoal",
            "description",
            "days_duration",
            "state",
            "priority",
            "slug"
        ]
        widgets = {
            "ggname": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el grantgoal name!"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "row": 3, "placeholder":"Escribe el grantgoal description!"}),
            "user": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "area": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "grantgoal": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "priority": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el grantgoal slug!"}),
        }




class UpdateSubGrantGoalForm(forms.ModelForm):
    class Meta:
        model = models.SubGrantGoal
        fields = [
            "sggname",
            "user",
            "area",
            "grantgoal",
            "description",
            "days_duration",
            "state",
            "priority",
            "slug"
        ]
        widgets = {
            "ggname": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el grantgoal name!"}),
            "description": forms.Textarea(attrs={"type":"text", "class":"form-control", "row": 3, "placeholder":"Escribe el grantgoal description!"}),
            "user": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "area": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "grantgoal": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "days_duration": forms.NumberInput(attrs={"type":"number", "class":"form-control"}),
            "priority": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "state": forms.Select(attrs={"type":"select", "class":"form-control"}),
            "slug": forms.TextInput(attrs={"type":"text", "class":"form-control", "placeholder":"Escribe el grantgoal slug!"}),
        }

### Goal forms
class NewGoalForm(forms.ModelForm):
    class Meta:
        model = models.Goal
        fields = [
            "goalname",
            "description",
            "area",
            "user",
            "subgrantgoal",
            "days_duration",
            "final_date",
            "progress",
            "priority",
            "state",
            "status",
            "slug"
        ]
        widgets = {
            "goalname": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Escribe el nombre del objetivo!"}),
            "description": forms.Textarea(attrs={"type": "text", "class": "form-control", "rows": 3, "placeholder": "Escribe la descripción del objetivo!"}),
            "area": forms.Select(attrs={"class": "form-control"}),
            "user": forms.Select(attrs={"class": "form-control"}),
            "subgrantgoal": forms.Select(attrs={"class": "form-control"}),
            "days_duration": forms.NumberInput(attrs={"class": "form-control"}),
            "final_date": forms.DateInput(attrs={"type": "date", "class": "form-control"}),
            "progress": forms.NumberInput(attrs={"type": "number", "class": "form-control", "step": "0.01", "min": "0", "max": "100"}),
            "priority": forms.Select(attrs={"class": "form-control"}),
            "state": forms.Select(attrs={"class": "form-control"}),
            "status": forms.CheckboxInput(attrs={"class": "form-check-input"}),
            "slug": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Escribe el identificador del objetivo!"}),
        }


class UpdateGoalForm(forms.ModelForm):
    class Meta:
        model = models.Goal
        fields = [
            "goalname",
            "description",
            "area",
            "user",
            "subgrantgoal",
            "days_duration",
            "final_date",
            "progress",
            "priority",
            "state",
            "status",
            "slug"
        ]
        widgets = {
            "goalname": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Escribe el nombre del objetivo!"}),
            "description": forms.Textarea(attrs={"type": "text", "class": "form-control", "rows": 3, "placeholder": "Escribe la descripción del objetivo!"}),
            "area": forms.Select(attrs={"class": "form-control"}),
            "user": forms.Select(attrs={"class": "form-control"}),
            "subgrantgoal": forms.Select(attrs={"class": "form-control"}),
            "days_duration": forms.NumberInput(attrs={"class": "form-control"}),
            "final_date": forms.DateInput(attrs={"type": "date", "class": "form-control"}),
            "progress": forms.NumberInput(attrs={"type": "number", "class": "form-control", "step": "0.01", "min": "0", "max": "100"}),
            "priority": forms.Select(attrs={"class": "form-control"}),
            "state": forms.Select(attrs={"class": "form-control"}),
            "status": forms.CheckboxInput(attrs={"class": "form-check-input"}),
            "slug": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Escribe el identificador del objetivo!"}),
        }


### Issues forms

class NewIssueForm(forms.ModelForm):
    class Meta:
        model = models.Issue
        fields = [
            "issuename",
            "description",
            "area",
            "user",
            "goal",
            "days_duration",
            "final_date",
            "progress",
            "priority",
            "state",
            "status",
            "slug"
        ]
        widgets = {
            "issuename": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Escribe el nombre del problema!"}),
            "description": forms.Textarea(attrs={"type": "text", "class": "form-control", "rows": 3, "placeholder": "Escribe la descripción del problema!"}),
            "area": forms.Select(attrs={"class": "form-control"}),
            "user": forms.Select(attrs={"class": "form-control"}),
            "goal": forms.Select(attrs={"class": "form-control"}),
            "days_duration": forms.NumberInput(attrs={"class": "form-control"}),
            "final_date": forms.DateInput(attrs={"type": "date", "class": "form-control"}),
            "progress": forms.NumberInput(attrs={"type": "number", "class": "form-control", "step": "0.01", "min": "0", "max": "100"}),
            "priority": forms.Select(attrs={"class": "form-control"}),
            "state": forms.Select(attrs={"class": "form-control"}),
            "status": forms.CheckboxInput(attrs={"class": "form-check-input"}),
            "slug": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Escribe el identificador del problema!"}),
        }


class UpdateIssueForm(forms.ModelForm):
    class Meta:
        model = models.Issue
        fields = [
            "issuename",
            "description",
            "area",
            "user",
            "goal",
            "days_duration",
            "final_date",
            "progress",
            "priority",
            "state",
            "status",
            "slug"
        ]
        widgets = {
            "issuename": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Escribe el nombre del problema!"}),
            "description": forms.Textarea(attrs={"type": "text", "class": "form-control", "rows": 3, "placeholder": "Escribe la descripción del problema!"}),
            "area": forms.Select(attrs={"class": "form-control"}),
            "user": forms.Select(attrs={"class": "form-control"}),
            "goal": forms.Select(attrs={"class": "form-control"}),
            "days_duration": forms.NumberInput(attrs={"class": "form-control"}),
            "final_date": forms.DateInput(attrs={"type": "date", "class": "form-control"}),
            "progress": forms.NumberInput(attrs={"type": "number", "class": "form-control", "step": "0.01", "min": "0", "max": "100"}),
            "priority": forms.Select(attrs={"class": "form-control"}),
            "state": forms.Select(attrs={"class": "form-control"}),
            "status": forms.CheckboxInput(attrs={"class": "form-check-input"}),
            "slug": forms.TextInput(attrs={"type": "text", "class": "form-control", "placeholder": "Escribe el identificador del problema!"}),
        }


### Area form

class NewAreaForm(forms.ModelForm):
    class Meta:
        model = models.Area
        fields = ['area_name', 'description']
        widgets = {
            'area_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter area name'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'rows': 3, 'placeholder': 'Enter description'}),
        }

class UpdateAreaForm(forms.ModelForm):
    class Meta:
        model = models.Area
        fields = ['area_name', 'description']
        widgets = {
            'area_name': forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'Enter area name'}),
            'description': forms.Textarea(attrs={'class': 'form-control', 'rows': 3, 'placeholder': 'Enter description'}),
        }